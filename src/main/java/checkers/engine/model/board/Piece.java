package checkers.engine.model.board;

import java.io.Serializable;

/**
 * <p>
 *     Enumeration class for the different types of pieces to
 *     set the state of the board.
 * </p>
 * @author Prakash on 8/28/17.
 */
public enum Piece implements Serializable {

    BLACK(false, true),
    RED(true, false),
    BLACK_KING(false, true) {
        @Override
        public final boolean isKing() {
            return true;
        }
    },
    RED_KING(true, false) {
        @Override
        public final boolean isKing() {
            return true;
        }
    },
    EMPTY(false, false) {
        @Override
        public boolean isEmpty() {
            return true;
        }
    }
    ;

    private final boolean red;
    private final boolean black;

    /**
     * Parametrized constructor to set the piece configuration
     * @param red whether the piece is RED
     * @param black whether the piece is BLACK
     */
    Piece(final boolean red, final boolean black) {
        this.red = red;
        this.black = black;
    }

    /**
     * Returns whether the piece is RED
     * @return boolean
     */
    public boolean isRed() {
        return red;
    }

    /**
     * Returns whether the piece is BLACK
     * @return boolean
     */
    public boolean isBlack() {
        return black;
    }

    /**
     * Returns whether there is no piece on the square
     * @return boolean
     */
    public boolean isEmpty() {
        return false;
    }

    /**
     * Returns whether the piece is a KING
     * @return boolean
     */
    public boolean isKing() {
        return false;
    }
}
