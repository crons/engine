package checkers.engine.model.board;

import checkers.engine.util.LogContext;
import checkers.engine.util.exception.MoveException;
import checkers.engine.model.move.Move;
import checkers.engine.model.move.Moves;
import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.Setter;

import java.awt.Point;
import java.io.Serializable;
import java.util.List;
import java.util.stream.IntStream;

/**
 * <p>
 *     Board for the Checkers game that contains the {@link Piece}(s) to
 *     dictate the state of the game.
 *     [ <a href="https://github.com/BalajiLaks/CS-451/blob/master/CheckersServer/src/Board.java">reference</a> ]
 * </p>
 * @author Prakash on 8/28/17.
 */
@Getter
@Setter
public class Board implements Serializable {

    private static final LogContext LOGGER = new LogContext(Board.class);
    private static final double SQRT2 = Math.sqrt(2);
    private static final double SQRT8 = Math.sqrt(8);

    public static final List<Piece> INITIAL_SETUP = ImmutableList.of(
            Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK,
            Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY,
            Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK, Piece.EMPTY, Piece.BLACK,
            Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY,
            Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY, Piece.EMPTY,
            Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY,
            Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED,
            Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY, Piece.RED, Piece.EMPTY
    );

    private final Piece[][] state;
    private Piece turn;
    private Piece winner;

    /**
     * Parametrized constructor that takes the {@link Piece}(s)
     * from the spring configuration and sets the state. <b> Also, the
     * turn is set to BLACK</b>
     */
    public Board() {
        LOGGER.info("Setting the board");
        state = new Piece[8][8];
        reset();
    }

    public Board(List<Piece> pieces) {
        LOGGER.info("Setting the board");
        state = new Piece[8][8];
        IntStream.range(0, pieces.size())
                .forEach( i -> state[i / 8][i % 8] = pieces.get(i));
        this.turn = Piece.BLACK;
        this.winner = Piece.EMPTY;
    }

    /**
     * Makes the singleton move and changes the state of the {@link Board}. The move is made
     * by the player whose turn it is. Must be called after the move has been validated.
     * @param move {@link Move} to be made
     * @throws MoveException In case there are operational errors while making the move.
     */
    private void makeMove(Move move) throws MoveException {
        Point start = move.getStartPoint();
        Point end = move.getEndPoint();
        double distance = start.distance(end);

        if (distance != SQRT2 && distance != SQRT8){
            LOGGER.warn("[Method] makeMove()");
            throw new MoveException(String.format("Invalid move %s -> %s",
                    start.toString(), end.toString()));
        }

        Piece c = state[start.x][start.y];
        if (!c.isRed() && !c.isBlack()) {
            LOGGER.warn(c.toString());
            LOGGER.warn(start.toString());
            throw new MoveException("Tried to move from location with no piece");
        } else if (c != turn) {
            LOGGER.warn("[Method] makeMove()");
            throw new MoveException(String.format("Turn violation"));
        } else if (c == Piece.RED && end.x == 0) {
            c = Piece.RED_KING;
        } else if (c == Piece.BLACK && end.x == 7){
            c = Piece.BLACK_KING;
        }

        state[start.x][start.y] = Piece.EMPTY;
        state[end.x][end.y] = c;

        if (distance == SQRT8) {
            if (start.x > end.x && start.y > end.y) {
                state[start.x-1][start.y-1] = Piece.EMPTY;
            } else if (start.x > end.x && start.y < end.y) {
                state[start.x-1][start.y+1] = Piece.EMPTY;
            } else if (start.x < end.x && start.y < end.y) {
                state[start.x+1][start.y+1] = Piece.EMPTY;
            } else if (start.x < end.x && start.y > end.y) {
                state[start.x+1][start.y-1] = Piece.EMPTY;
            }
        }
    }

    /**
     * Makes the {@link Moves} made by the payer with the current turn and
     * then switches the turn.
     * @param moveSequence {@link Moves}
     * @throws MoveException In case of moves not possible.
     */
    public void doTurn (Moves moveSequence) throws MoveException {
        List<Move> moves = moveSequence.getMoves();
        for (Move move: moves) {
            makeMove(move);
        }
        if (turn.isBlack()) {
            turn = Piece.RED;
        } else {
            turn = Piece.BLACK;
        }
    }

    /**
     * Gets a copy of the state
     * @return Array of {@link Piece}
     */
    public Piece[][] getState() {
        Piece[][] copy = new Piece[8][8];
        IntStream.range(0,8)
                .forEach(i -> copy[i] = state[i].clone());
        return copy;
    }

    /**
     * Resets the board to the initial state
     * @return {@link Board}
     */
    public Board reset() {
        LOGGER.info("Re-setting the board");
        IntStream.range(0, INITIAL_SETUP.size())
                .forEach( i -> state[i / 8][i % 8] = INITIAL_SETUP.get(i));
        this.turn = Piece.BLACK;
        this.winner = Piece.EMPTY;
        return this;
    }
}
