package checkers.engine.model.move;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class MovesTest {

    @Mock
    private Move move;

    private Moves moves, nullMoves;

    @Before
    public void setUp() throws Exception {
        moves = new Moves(move);
        nullMoves = new Moves();
    }

    @Test
    public void getMoves() throws Exception {
        assertNotNull(moves.getMoves());
        assertEquals(moves.getMoves().size(), 1, 0.0);
        assertNotNull(nullMoves.getMoves());
        assertEquals(nullMoves.getMoves().size(), 0, 0.0);
    }

}