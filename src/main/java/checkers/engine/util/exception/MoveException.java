package checkers.engine.util.exception;

/**
 * <p>
 *     Errors while making the {@link checkers.engine.model.move.Move} are represented
 *     with this exception.
 * </p>
 * @author Prakash on 8/28/17.
 */
public class MoveException extends UnsupportedException {

    /**
     * Parametrized exception to bind the message
     * @param message message for the exception
     */
    public MoveException(String message) {
        super(message);
    }
}
