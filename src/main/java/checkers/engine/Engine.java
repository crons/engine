package checkers.engine;

import checkers.engine.util.LogContext;
import checkers.engine.util.Server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 *      Driver class for the Rule Engine to run on the server
 *      and accept connections from the client.
 *      [ <a href="https://github.com/BalajiLaks/CS-451/blob/master/CheckersServer/src/Server.java">reference</a> ]
 * </p>
 * @author Prakash on 08/28/17
 */
public class Engine {

    private final static LogContext LOGGER = new LogContext(Engine.class);

    public static void main(String args[]) throws Exception {
        ServerSocket socket = new ServerSocket(8080);
        LOGGER.debug("Listening on port 8080");

        List<Thread> clients  = new LinkedList<>();
        Socket sock;

        while (true) {
            sock = socket.accept();
            if (clients.size() == 0) {
                LOGGER.info("Connected: " + sock.getInetAddress());
                clients.add(new Thread(new Server(sock)));
                clients.get(0).start();
            } else if (clients.size() == 1) {
                if (clients.get(0).isAlive()) {
                    LOGGER.info("Connected: " + sock.getInetAddress());
                    clients.add(new Thread(new Server(sock)));
                    clients.get(1).start();

                    for (Thread client : clients) {
                        client.join();
                        LOGGER.debug("Joined the two Clients");
                    }
                    clients.clear();
                } else {
                    clients.clear();
                    clients.add(new Thread(new Server(sock)));
                    clients.get(0).start();
                }
            } else {
                LOGGER.info("Rejected: " + sock.getInetAddress());
                sock.close();
            }
        }
    }

}
