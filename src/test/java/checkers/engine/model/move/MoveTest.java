package checkers.engine.model.move;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MoveTest {

    private Move move;

    @Before
    public void setUp() throws Exception {
        move = new Move(1, 2, 3, 4);
    }

    @Test
    public void getStartPoint() throws Exception {
        assertNotNull(move.getStartPoint());
    }

    @Test
    public void getEndPoint() throws Exception {
        assertNotNull(move.getEndPoint());
    }

}