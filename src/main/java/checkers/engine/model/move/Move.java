package checkers.engine.model.move;

import lombok.Getter;

import java.awt.Point;
import java.io.Serializable;

/**
 * <p>
 *     Abstraction to represent the Checkers Move to handle the
 *     move from different players.
 *     [ <a href="https://github.com/BalajiLaks/CS-451/blob/Joe/CheckersServer/src/Move.java">reference</a> ]
 * </p>
 * @author Prakash on 8/21/17.
 */
@Getter
public class Move implements Serializable {

    private final Point startPoint;
    private final Point endPoint;

    /**
     * Parametrized constructor to set the start and the end {@link Point}(s)
     * for the {@link Move}
     * @param startX X co-ordinate of the start position
     * @param startY Y co-ordinate of the start position
     * @param endX X co-ordinate of the end position
     * @param endY Y co-ordinate of the end position
     */
    public Move(
            Integer startX,
            Integer startY,
            Integer endX,
            Integer endY) {
        startPoint = new Point(startX, startY);
        endPoint = new Point(endX, endY);
    }

}
