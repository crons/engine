package checkers.engine.util;

import checkers.engine.util.exception.UnsupportedException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogContextTest {

    private LogContext context;

    @Before
    public void setup() throws Exception {
        context = new LogContext(LogContextTest.class);
    }

    @Test
    public void error() throws Exception {
        context.withContext("Method", "error")
                .addContext("Error", "sample");
        context.error(new UnsupportedException());
    }

    @Test
    public void info() throws Exception {
        context.info("sample");
    }

    @Test
    public void debug() throws Exception {
        context.debug("sample");
    }

    @Test
    public void warn() throws Exception {
        context.warn("sample");
    }

}