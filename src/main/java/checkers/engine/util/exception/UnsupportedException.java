package checkers.engine.util.exception;

/**
 * <p>
 *     Represents the operations that are not supported by the calling
 *     instance.
 * </p>
 * @author Prakash on 8/28/17.
 */
public class UnsupportedException extends Exception {

    private static final long serialVersionUID = 7L;

    /**
     * Default constructor
     */
    public UnsupportedException() {
    }

    /**
     * Parametrized exception to bind the message
     * @param message message for the exception
     */
    public UnsupportedException(String message) {
        super(message);
    }
}
