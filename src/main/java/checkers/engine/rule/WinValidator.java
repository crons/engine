package checkers.engine.rule;

import checkers.engine.util.LogContext;
import checkers.engine.model.board.Board;
import checkers.engine.model.board.Piece;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * <p>
 *     Abstraction to validate whether the {@link Board} represents a
 *     winning situation.
 *     [ <a href="https://github.com/BalajiLaks/CS-451/blob/master/CheckersServer/src/Board.java">reference</a> ]
 * </p>
 * @author Prakash on 8/28/17.
 */
@Getter
@RequiredArgsConstructor
public class WinValidator {

    private final static LogContext LOGGER = new LogContext(WinValidator.class);

    /**
     * Validates whether the state for the {@link Board} represents a winning situation or not.
     * @param board {@link Board}
     * @return boolean
     */
    public static boolean validate(Board board) {
        long black = Arrays.stream(board.getState())
                .flatMap(Arrays::stream)
                .parallel()
                .filter(Piece::isBlack)
                .count();
        long red = Arrays.stream(board.getState())
                .flatMap(Arrays::stream)
                .parallel()
                .filter(Piece::isRed)
                .count();
        if (black == 0) {
            board.setWinner(Piece.RED);
            return true;
        } else if (red == 0) {
            board.setWinner(Piece.BLACK);
            return true;
        }
        return false;
    }

}
