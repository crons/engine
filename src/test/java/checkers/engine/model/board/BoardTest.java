package checkers.engine.model.board;

import checkers.engine.util.exception.MoveException;
import checkers.engine.model.move.Move;
import checkers.engine.model.move.Moves;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BoardTest {

    private Board board;

    @Before
    public void setup() throws Exception {
        board= new Board();
    }

    @Test(expected = MoveException.class)
    public void doTurnInvalidMoveException() throws Exception {
        Move move = new Move(0,0,0,1);
        board.doTurn(new Moves(move));
    }

    @Test(expected = MoveException.class)
    public void doTurnIEmptyException() throws Exception {
        Move move = new Move(0,0,1,1);
        board.doTurn(new Moves(move));
    }

    @Test(expected = MoveException.class)
    public void doTurnException() throws Exception {
        Move move = new Move(5,0,4,1);
        board.doTurn(new Moves(move));
    }

    @Test
    public void doTurn() throws Exception {
        Move move = new Move(2,3,3,4);
        board.doTurn(new Moves(move));
        assertEquals(board.getTurn(), Piece.RED);
        move = new Move(5, 0, 4, 1);
        board.doTurn(new Moves(move));
        assertEquals(board.getTurn(), Piece.BLACK);
    }

    @Test
    public void getState() throws Exception {
        assertNotNull(board.getState());
    }

    @Test
    public void getTurn() throws Exception {
        assertEquals(board.getTurn(), Piece.BLACK);
        assertEquals(board.setTurn(Piece.RED).getTurn(), Piece.RED);
    }

    @Test
    public void getWinner() throws Exception {
        assertEquals(board.getWinner(), Piece.EMPTY);
        assertEquals(board.setWinner(Piece.BLACK).getWinner(), Piece.BLACK);
    }

    @Test
    public void reset() throws Exception {
        Board test = new Board();
        board.reset();
        test.reset();
        assertArrayEquals(board.getState(), test.getState());
    }
}