package checkers.engine.model.board;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class PieceTest {

    @Parameterized.Parameter
    public Piece piece;

    @Parameterized.Parameter(value = 1)
    public boolean red;

    @Parameterized.Parameter(value = 2)
    public boolean black;

    @Parameterized.Parameter(value = 3)
    public boolean empty;

    @Parameterized.Parameter(value = 4)
    public boolean king;

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> data() {
        return Collections.unmodifiableList(
                Arrays.asList(new Object[][] {
                        {Piece.BLACK, false, true, false, false},
                        {Piece.RED, true, false, false, false},
                        {Piece.BLACK_KING, false, true, false, true},
                        {Piece.RED_KING, true, false, false, true},
                        {Piece.EMPTY, false, false, true, false}
                })
        );
    }

    @Test
    public void isRed() throws Exception {
        assertEquals(piece.isRed(), red);
    }

    @Test
    public void isBlack() throws Exception {
        assertEquals(piece.isBlack(), black);
    }

    @Test
    public void isEmpty() throws Exception {
        assertEquals(piece.isEmpty(), empty);
    }

    @Test
    public void isKing() throws Exception {
        assertEquals(piece.isKing(), king);
    }

}