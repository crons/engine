package checkers.engine.util;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.util.Objects;

/**
 * <p>
 *     Abstraction over the basic slf4j logging to add critical
 *     information while logging the exceptions
 * </p>
 * @author Prakash on 6/4/17.
 */
public class LogContext {

    private StringBuilder context;
    private transient final Logger LOGGER;

    /**
     * Sets the logger to the specified class
     * @param clazz Class name specified
     */
    public LogContext(Class clazz) {
        context = new StringBuilder();
        context.append("\n\n[ Class ]  " + clazz.getName());
        LOGGER = Logger.getLogger(clazz);
    }

    /**
     * Adds the context to the logger in the form of [ key] value
     * @param key String
     * @param value {@link Object}
     */
    public void addContext(String key, Object value) {
        context.append("\n");
        context.append("[ " + key + " ]  ");
        context.append(Objects.toString(value));
    }

    /**
     * Returns the object after adding the context
     * @param key String
     * @param value {@link Object}
     * @return {@link LogContext}
     */
    public LogContext withContext(String key, Object value) {
        context.append("\n");
        context.append("[ " + key + " ]  ");
        context.append(Objects.toString(value));
        return this;
    }

    /**
     * Logs the errors/exceptions
     * @param cause Throwable
     */
    public void error(Throwable cause) {
        BasicConfigurator.configure();
        if (cause != null) {
            context.append("\n[ ERROR ]  " + cause.toString());
        }
        context.append("\n");
        LOGGER.error(context.toString(), cause);
    }

    /**
     * Logs the information to the console
     * @param message Message for the information
     */
    public void info(String message) {
        BasicConfigurator.configure();
        LOGGER.info(message);
    }

    /**
     * Logs the debug message
     * @param message Message
     */
    public void debug(String message) {
        BasicConfigurator.configure();
        LOGGER.debug(message);
    }

    /**
     * Logs the warnings
     * @param message Message for the warning
     */
    public void warn(String message) {
        BasicConfigurator.configure();
        LOGGER.warn(message);
    }
}

