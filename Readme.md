# Release Notes

#### v1.0.RELEASE Final Release
##### What's new:
* fixed: some bugs like added the reeset functionality to Board class
* fixed: Added the project to automated pipeline deployment
* fixed: Created the release version artifact for the game.
* fixed: uploaded the docs to S3 bucket.

#### Previous releases:
##### v1.0-SNAPSHOT
* fixed: Added the documentation
* fixed: Added the jacoco and findbugs integrations
* fixed: Replaced deprecated codes with Java 8 API for efficiency