package checkers.engine.rule;

import checkers.engine.util.exception.UnsupportedException;
import checkers.engine.model.board.Board;
import checkers.engine.model.board.Piece;
import checkers.engine.model.move.Move;
import checkers.engine.model.move.Moves;

import java.awt.Point;
import java.util.List;

/**
 * <p>
 *     Abstraction to ensure that the {@link Moves} submitted by the player
 *     is valid for the given state.
 *     [ <a href="https://github.com/BalajiLaks/CS-451/blob/master/CheckersServer/src/Board.java">reference</a> ]
 * </p>
 * @author Prakash on 8/28/17.
 */
public class MoveValidator {

    private static final double SQRT2 = Math.sqrt(2);
    private static final double SQRT8 = Math.sqrt(8);

    /**
     * Validate the {@link Moves} submitted by the player. Critical to call this before the
     * moves are applied.
     * @param moveSequence {@link Moves} instance
     * @param checkerBoard {@link Board}
     * @return boolean
     * @throws UnsupportedException Should not throw exception in this case.
     */
    public static boolean validate(Moves moveSequence, Board checkerBoard) throws UnsupportedException {
        Piece[][] board = checkerBoard.getState();
        List<Move> moves = moveSequence.getMoves();
        Point start, end;
        if (moves.size() == 0) {
            return false;
        } else if (moves.size() == 1){
            start = moves.get(0).getStartPoint();
            end = moves.get(0).getEndPoint();
            if (start.distance(end) == SQRT2) {
                return isValidMove(moves.get(0), board[start.x][start.y], checkerBoard);
            } else if (start.distance(end) == SQRT8) {
                return isValidEndMove(moves.get(0), board[start.x][start.y], checkerBoard);
            } else {
                return false;
            }
        }
        else {
            start = moves.get(0).getStartPoint();
            Piece startPlayer = board[start.x][start.y];
            if (!isValidMove(moves.get(0), startPlayer, checkerBoard)) {
                return false;
            }
            for (int i = 1; i< moves.size(); i++) {
                start = moves.get(i).getStartPoint();
                end = moves.get(i).getEndPoint();

                if (start.distance(end) != SQRT8) {
                    return false;
                } else {
                    if (i == moves.size() - 1) {
                        if (!isValidEndMove(moves.get(i),startPlayer, checkerBoard)) {
                            return false;
                        }
                    } else if (!isValidMove(moves.get(i), startPlayer, checkerBoard)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Validates if the singleton {@link Move} is valid under the current state
     * of the {@link Board}
     * @param move {@link Move} submitted
     * @param startPlayer {@link Piece} applying the move
     * @param checkerBoard {@link Board}
     * @return
     */
    private static boolean isValidMove(Move move, Piece startPlayer, Board checkerBoard) {
        Point start = move.getStartPoint();
        Point end = move.getEndPoint();
        double distance  = start.distance(end);
        Piece[][] board = checkerBoard.getState();
        if (startPlayer != checkerBoard.getTurn()) { //not moving your own piece
            return false;
        }
        else if (!board[end.x][end.y].isEmpty()) { //end location isn't Piece.EMPTY
            return false;
        }
        else if (startPlayer == Piece.RED && start.x < end.x 		//disallow non-king pieces from
                || startPlayer == Piece.BLACK && end.x < start.x) { //moving in wrong direction
            return false;
        }
        else if (distance == SQRT8) {
            //stop players from jumping over their own pieces or Piece.EMPTY locations
            if (start.x > end.x && start.y > end.y) {
                if (board[start.x-1][start.y-1] == checkerBoard.getTurn()
                        || board[start.x-1][start.y-1].isEmpty()) {
                    return false;
                }
            }
            else if (start.x > end.x && start.y < end.y) {
                if (board[start.x-1][start.y+1] == checkerBoard.getTurn()
                        || board[start.x-1][start.y+1].isEmpty()) {
                    return false;
                }
            }
            else if (start.x < end.x && start.y < end.y) {
                if (board[start.x+1][start.y+1] == checkerBoard.getTurn()
                        || board[start.x+1][start.y+1].isEmpty()) {
                    return false;
                }
            }
            else if (start.x < end.x && start.y > end.y) {
                if (board[start.x+1][start.y-1] == checkerBoard.getTurn()
                        || board[start.x+1][start.y-1].isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns whether a given move is valid without within rules of checkers including
     * checking if the move should fail because more jumps can be made
     * @param move the move being checked
     * @param startPlayer Player that applies the move, i.e, {@link Piece}
     * @param checkerBoard {@link Board}
     * @return whether move is valid in this context
     */
    private static boolean isValidEndMove(Move move, Piece startPlayer, Board checkerBoard) {
        Piece[][] board = checkerBoard.getState();
        if (!isValidMove(move, startPlayer, checkerBoard))
            return false;
        else {
            Point start = move.getStartPoint();
            Point end = move.getEndPoint();

            //check if there are still moves the player can make
            if (startPlayer == Piece.RED) {
                if (end.y - 2 >= 0 && end.x - 2 >= 0 && board[end.x - 1][end.y - 1].isBlack()
                        && board[end.x - 2][end.y - 2].isEmpty()) {
                    return false;
                } else if (end.x - 2 >= 0 && end.y + 2 <= 7 && board[end.x - 1][end.y + 1].isBlack()
                        && board[end.x - 2][end.y + 2].isEmpty()) {
                    return false;
                }
            }
            else if (startPlayer == Piece.BLACK) {
                if (end.x + 2 <= 7 && end.y + 2 <= 7 && board[end.x + 1][end.y + 1].isRed()
                        && board[end.x + 2][end.y + 2].isEmpty()) {
                    return false;
                } else if (end.x + 2 <= 7 && end.y - 2 >= 0 && board[end.x + 1][end.y - 1].isRed()
                        && board[end.x + 2][end.y - 2].isEmpty()) {
                    return false;
                }
            }


            if (startPlayer.isKing()) {
                if (start.x > end.x && start.y > end.y) {
                    if (end.x+2 <= 7 && end.y-2 >=0 && board[end.x+1][end.y-1] == switchColor(startPlayer)
                            && board[end.x+2][end.y-2].isEmpty()) {
                        return false;
                    } else if (end.y - 2 >= 0 && end.x - 2 >= 0 && board[end.x - 1][end.y - 1] == switchColor(startPlayer)
                            && board[end.x - 2][end.y - 2].isEmpty()) {
                        return false;
                    } else if (end.x - 2 >= 0 && end.y + 2 <= 7 && board[end.x - 1][end.y + 1] == switchColor(startPlayer)
                            && board[end.x - 2][end.y + 2].isEmpty()) {
                        return false;
                    }
                } else if (start.x > end.x && start.y < end.y) {
                    if (end.x+2 <= 7 && end.y+2 <=7 && board[end.x+1][end.y+1] == switchColor(startPlayer)
                            && board[end.x+2][end.y+2].isEmpty()){
                        return false;
                    } else if (end.y - 2 >= 0 && end.x - 2 >= 0 && board[end.x - 1][end.y - 1] == switchColor(startPlayer)
                            && board[end.x - 2][end.y - 2].isEmpty()) {
                        return false;
                    } else if (end.x - 2 >= 0 && end.y + 2 <= 7 && board[end.x - 1][end.y + 1] == switchColor(startPlayer)
                            && board[end.x - 2][end.y + 2].isEmpty()) {
                        return false;
                    }
                } else if (start.x < end.x && start.y < end.y) {
                    if (end.x+2 <= 7 && end.y+2 <=7 && board[end.x+1][end.y+1] == switchColor(startPlayer)
                            && board[end.x+2][end.y+2].isEmpty()) {
                        return false;
                    } else if (end.x+2 <= 7 && end.y-2 >=0 && board[end.x+1][end.y-1] == switchColor(startPlayer)
                            && board[end.x+2][end.y-2].isEmpty()) {
                        return false;
                    } else if (end.x - 2 >= 0 && end.y + 2 <= 7 && board[end.x - 1][end.y + 1] == switchColor(startPlayer)
                            && board[end.x - 2][end.y + 2].isEmpty()) {
                        return false;
                    }
                } else if (start.x < end.x && start.y > end.y) {
                    if (end.x+2 <= 7 && end.y+2 <=7 && board[end.x+1][end.y+1] == switchColor(startPlayer)
                            && board[end.x+2][end.y+2].isEmpty()) {
                        return false;
                    } else if (end.y - 2 >= 0 && end.x - 2 >= 0 && board[end.x - 1][end.y - 1] == switchColor(startPlayer)
                            && board[end.x - 2][end.y - 2].isEmpty()) {
                        return false;
                    } else if (end.x+2 <= 7 && end.y-2 >=0 && board[end.x+1][end.y-1] == switchColor(startPlayer)
                            && board[end.x+2][end.y-2].isEmpty()){
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Switches the color for validation purposes.
     * @param piece {@link Piece} with color
     * @return {@link Piece}
     */
    private static Piece switchColor(Piece piece) {
        if (piece.isBlack())
            return Piece.RED;
        else
            return Piece.BLACK;
    }
}
