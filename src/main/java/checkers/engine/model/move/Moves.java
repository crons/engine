package checkers.engine.model.move;

import com.google.common.collect.ImmutableList;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 *     Abstraction for Moves to get the basic moves made by the
 *     player through the server.
 *     [ <a href="https://github.com/BalajiLaks/CS-451/blob/master/CheckersServer/src/MoveSequence.java">reference</a> ]
 * </p>
 * @author Prakash on 8/27/17.
 */
@Getter
public class Moves implements Serializable {

    private final List<Move> moves;

    /**
     * Parametrized constructor to set the moves
     * @param moves Sequence of {@link Move}
     */
    @SuppressWarnings("unchecked")
    public Moves(Move... moves) {
        if (moves == null || moves.length == 0) {
            this.moves =  Collections.EMPTY_LIST;
        } else {
            this.moves =  ImmutableList.copyOf(moves);
        }
    }

}
