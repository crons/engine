package checkers.engine.rule.validator;

import checkers.engine.rule.MoveValidator;
import checkers.engine.model.board.Board;
import checkers.engine.model.move.Move;
import checkers.engine.model.move.Moves;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class MoveValidatorTest {

    private Board board;

    @Before
    public void setup() {
        board = new Board();
    }

    @Test
    public void validate() throws Exception {
        Move move = new Move(5, 0, 4, 1);
		assertFalse(MoveValidator.validate(new Moves(move), board));

		move = new Move(2, 1, 2, 1);
		assertFalse(MoveValidator.validate(new Moves(move), board));

        move = new Move(0,1,3,2);
        Move move1 = new Move(5,0,4,1);
		assertFalse(MoveValidator.validate(new Moves(move, move1), board));

		move = new Move(2, 1, 4, 3);
        assertFalse(MoveValidator.validate(new Moves(move), board));
    }

}