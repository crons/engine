package checkers.engine.util;

import checkers.engine.model.board.Board;
import checkers.engine.model.move.Moves;
import checkers.engine.rule.MoveValidator;
import checkers.engine.rule.WinValidator;
import checkers.engine.util.exception.MoveException;
import checkers.engine.util.exception.UnsupportedException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 *     Server that represents a player for the checkers game and
 *     keeps the connection alive.
 *     [ <a href="https://github.com/BalajiLaks/CS-451/blob/master/CheckersServer/src/ServerThread.java">reference</a> ]
 * </p>
 * @author Prakash on 8/28/17.
 */
public class Server implements Runnable {

    private final static LogContext LOGGER = new LogContext(Server.class);
    private static volatile Board board = new Board();
    private final Socket socket;

    private static AtomicInteger numConnected = new AtomicInteger(0);
    private static AtomicBoolean hasForfeited =  new AtomicBoolean(false);
    private static AtomicBoolean isWon =  new AtomicBoolean(false);
    private static AtomicBoolean isTurnOver =  new AtomicBoolean(false);

    public Server(Socket socket) {
        this.socket = socket;
    }

    public void run()  {
        ObjectOutputStream out;
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            out.flush();
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            numConnected.getAndAdd(1);
            do {
                if (isWon.get()) {
                    numConnected.getAndAdd(-1);
                    if (numConnected.get() == 0) {
                        isWon.set(false);
                    }
                } else if (hasForfeited.get()) {
                    LOGGER.debug("Number of connections: " + numConnected.getAndAdd(-1));
                    if (numConnected.get() == 2) {
                        numConnected.getAndAdd(-1);
                        break;
                    } else {
                        LOGGER.info("Closing other connection");
                        out.writeObject("forfeit");
                        if (!isWon.get()) {
                            board.reset();
                        }
                        hasForfeited.set(false);
                    }
                } else if (numConnected.get() == 1) {
                    out.writeObject("waitscreen");
                    Thread.sleep(1000);
                    if (!isWon.get()) {
                        board.reset();
                    }
                } else if (numConnected.get() == 2) {
                    out.writeObject(board);
                    while (numConnected.get() == 2) {
                        Object obj = in.readObject();
                        if (obj instanceof String) {
                            String s = (String) obj;
                            if("ping".equals(s)) {
                                if(!isTurnOver.get() && !hasForfeited.get()) {
                                    out.writeObject("ping");
                                } else if (isTurnOver.get()){
                                    out.reset();
                                    out.writeObject(board);
                                    isTurnOver.set(false);
                                    if (isWon.get()) {
                                        break;
                                    }
                                } else if (hasForfeited.get()) {
                                    LOGGER.info("Closing other connection");
                                    out.writeObject("forfeit");
                                    numConnected.getAndAdd(-1);
                                    hasForfeited.set(false);
                                }
                            }
                        } else if (obj instanceof Moves) {
                            if (hasForfeited.get()) {
                                LOGGER.info("Closing other connection");
                                out.writeObject("forfeit");
                                numConnected.getAndAdd(-1);
                                hasForfeited.set(false);
                                break;
                            } else {
                                Moves moves = (Moves) obj;
                                if (MoveValidator.validate(moves, board)) {
                                    board.doTurn(moves);
                                    out.reset();
                                    out.writeObject(board);
                                    isTurnOver.set(true);
                                    if (WinValidator.validate(board)) {
                                        isWon.set(true);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } while (numConnected.get() > 0);
        } catch (IOException e) {
            LOGGER.info("Connection to client lost");
            if (numConnected.get() == 2)
                hasForfeited.set(true);
            if (numConnected.get() > 0)
                numConnected.getAndAdd(-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            LOGGER.warn("Unknown object read");
            e.printStackTrace();
        } catch (MoveException e) {
            LOGGER.warn("Move Validation must be broken :(");
            e.printStackTrace();
        } catch (UnsupportedException e) {
            LOGGER.warn("used an unsupported operation");
            e.printStackTrace();
        }
    }
}
